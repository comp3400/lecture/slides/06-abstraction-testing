## Testing { data-transition="fade none" }

---

###### Traditional Unit Testing

* Traditional unit testing specifies a single anecdote or *assertion* that holds true
* For example, we might plug the values `2` and `3` into the `add` function
  * `add 2 3 == 5`
  * We assert that the value `5` always comes back

---

###### Traditional Unit Testing

* Or slightly less trivial examples
  * `reverse([1,2,3,4]) == [4,3,2,1]`
  * `fmap (+1) [10,20,30] == [11,21,31]`

---

###### Automated Specification-Based Testing

* Automated testing uses *universal quantification* on one or more values
* We specify *properties*, but starting with *"for all..."*
* For example
  * `forall x. add x 0 == x`
  * `forall f. fmap f [1,2,3] == [f 1, f 2, f 3]`

---

###### Automated Specification-Based Testing

* We can check we have satisfied type-class laws with automated testing
* `forall x. fmap (\a -> a) x == x`
* `forall f g x. fmap f (fmap g x) == fmap (\a -> f (g a)) x`

---

###### Automated Specification-Based Testing

* The Haskell syntax for "forall" is lambda
* `\x -> fmap (\a -> a) x == x`
* `\f g x -> fmap f (fmap g x) == fmap (\a -> f (g a)) x`
* We pass these functions to a test library
* The test library will try values and check that the property holds true

---

###### Automated Specification-Based Testing

* Automated test libraries have *many* other features to help you test/debug a program
  * Shrinking on falsification *(finding the smallest counter-example)*
  * Implementing a value generator for your own data type
  * Generating functions
  * Re-running tests with the same generated values

---

###### Automated Specification-Based Testing for COMP3400

* There exist third-party automated testing libraries
  * [hedgehog](https://hackage.haskell.org/package/hedgehog)
  * [QuickCheck](https://hackage.haskell.org/package/QuickCheck)
  * **However, use of these libraries will typically require using a build tool**
  * Build tools are not covered in COMP3400 and no support is provided

---

###### Automated Specification-Based Testing for COMP3400

* You may use a third-party build tool at your own discretion
  * **However, be very careful in ensuring your build works on all systems**
* GHC is the only supported requirement for COMP3400
* Alternatively, there exists a *mostly complete* automated test library for COMP3400
  * [COMP3400 test-framework](https://gitlab.com/comp3400/test-framework/)
  * it contains a single source file
  * simply copy and paste it into a file `Test/Framework.hs`
  * then use `import Test.Framework` from your own source file

---

###### Automated Specification-Based Testing for COMP3400

<div style="font-size: large; text-align: center">Please experiment with the COMP3400 test framework</div>

<pre><code data-trim data-noescape>
% ghci Test/Framework.hs
GHCi, version 8.6.5: http://www.haskell.org/ghc/  :? for help
> quickCheck (\x -> reverse [x] == [x])
Passed 100 tests.
> quickCheck (\x y -> reverse [x, y] == [y, x])
Passed 100 tests.
> quickCheck (\x -> x + 1 == x)
Falsifiable after 0 tests:
-3
> quickCheck (\x -> x * 1 == x)
Passed 100 tests.
> check (quick { maxTest = 5000 }) (\x y -> x + y == y + x)
Passed 5000 tests.
</code></pre>

---

###### Automated Specification-Based Testing for COMP3400

<div style="font-size: large; text-align: center">Please experiment with the COMP3400 test framework</div>
<div style="font-size: large; text-align: center"><code>:{ This is GHCi multi-line :}</code></div>


<pre><code data-trim data-noescape>
> data Person = Person Int String deriving Show
> -- ↓ examine closely what is going on here ↓
> :{
| instance Arbitrary Person where
|   arbitrary = Person <$> arbitrary <*> arbitrary
| :}
> age (Person a _) = a
> birthday (Person a s) = Person (a + 1) s
> quickCheck (\p -> age (birthday p) > age p)
Passed 100 tests.
</code></pre>

