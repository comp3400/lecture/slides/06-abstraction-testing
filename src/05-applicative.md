## Functor and things { data-transition="fade none" }

###### The `Functor` type-class

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Functor k where
  fmap :: (a -> b) -> k a -> k b

-- instances
instance Functor Optional where
instance Functor List where
instance Functor ((->) r) where
instance Functor ((,) a) where
...lots

-- derived operations
flip :: Functor k => k (a -> b) -> a -> k b
anonfmap :: Functor k => b -> k a -> k b
...not many
</code></pre>

---

###### The `Functor` type-class

* The `Functor` type-class tends to have
  * lots of instances
  * few derived operations
* The `Functor` type-class does not have a superclass
* `Functor` sits at the top of a hierarchy
* Can we tighten the constraint in a useful way?
  * Fewer instances
  * More derived operations

---

###### A motivating example

<div style="font-size: large; text-align: center">Given a function <code>func</code>, and a lists <code>list</code>, loop through <code>list</code>, and apply the function to the element at that position.</div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, list) {
  var r = List.empty
  for(var i = 0; i &lt; list1.length; i++) {
    r += func(list[i])
  }
  return r
}
</code></pre>

---

###### A motivating example

<div style="font-size: large; text-align: center">Given a function <code>func</code>, and a lists <code>list</code>, loop through <code>list</code>, and apply the function to the element at that position.</div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, list) {
  var r = List.empty
  for(var i = 0; i &lt; list1.length; i++) {
    r += func(list[i])
  }
  return r
}
</code></pre>

* This example using "traditional code" clearly corresponds to `fmap` on lists
* `given func list = fmap func list`

---

###### Another motivating example

<div style="font-size: large; text-align: center">Given a function <code>func</code>, and a possibly null value <code>x</code>, return <code>null</code> or apply the function to <code>x</code></div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, x) {
  if(x == null)
    return null
  else
    return func(x)
}
</code></pre>

---

###### Another motivating example

<div style="font-size: large; text-align: center">Given a function <code>func</code>, and a possibly null value <code>x</code>, return <code>null</code> or apply the function to <code>x</code></div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, x) {
  if(x == null)
    return null
  else
    return func(x)
}
</code></pre>

* This example using "traditional code" clearly corresponds to `fmap` on optional
* `given func x = fmap func x`

---

###### Another motivating example

<div style="font-size: small; text-align: center">Given a function <code>func</code>, and two lists <code>list1</code> and <code>list2</code>, loop first through <code>list1</code>, then <code>list2</code> and apply the function to the element at that position.</div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, list1, list2) {
  var r = List.empty
  for(var i = 0; i &lt; list1.length; i++) {
    for(var j = 0; j &lt; list2.length; j++) {
      r += func(list1[i], list2[j])
    }
  }
  return r
}
</code></pre>

* Is `fmap` sufficient here?

---

###### Another motivating example

<div style="font-size: small; text-align: center">Given a function <code>func</code>, and two lists <code>list1</code> and <code>list2</code>, loop first through <code>list1</code>, then <code>list2</code> and apply the function to the element at that position.</div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, list1, list2) {
  var r = List.empty
  for(var i = 0; i &lt; list1.length; i++) {
    for(var j = 0; j &lt; list2.length; j++) {
      r += func(list1[i], list2[j])
    }
  }
  return r
}
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
> given (+) [1,2,3] [40,50,60]
[41,51,61,42,52,62,43,53,63]
> given (*) [1,2,3] [40,50,60]
[40,50,60,80,100,120,120,150,180]
</code></pre>

---

###### Another motivating example

<div style="font-size: large; text-align: center">Given a function <code>func</code>, and two values <code>x</code> and <code>y</code>, if either are <code>null</code> return <code>null</code>, otherwise apply the function</div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, x, y) {
  if(x == null)
    return null
  else if (y == null)
    return null
  else
    return func(x, y)
}
</code></pre>

* Is `fmap` sufficient here?

---

###### Another motivating example

<div style="font-size: large; text-align: center">Given a function <code>func</code>, and two values <code>x</code> and <code>y</code>, if either are <code>null</code> return <code>null</code>, otherwise apply the function</div>

<pre><code class="language-csharp" data-trim data-noescape>
given(func, x, y) {
  if(x == null)
    return null
  else if (y == null)
    return null
  else
    return func(x, y)
}
</code></pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
> given (+) Empty (Full 7)
Empty
> given (+) (Full 8) Empty
Empty
> given (+) (Full 8) (Full 7)
Full 15
</code></pre>

---

###### Another motivating example

<div style="font-size: small; text-align: center">Given a function <code>func</code>, and two values <code>x</code> and <code>y</code>, if either are <code>null</code> return <code>null</code>, otherwise apply the function</div>

<div style="font-size: small; text-align: center">Given a function <code>func</code>, and two lists <code>list1</code> and <code>list2</code>, loop first through <code>list1</code>, then <code>list2</code> and apply the function to the element at that position.</div>

* `fmap` is **insufficient** to complete these two use-cases

---

###### Some more operations

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)

-- apply every function (a -> b) to every value (a)
-- collect the results
applyList :: List (a -> b) -> List a -> List b
applyList Nil _ = Nil
applyList (Cons h t) x = fmap h x ++ applyList t x

lift0List :: a -> List a
lift0List a = Cons a Nil
</code></pre>

---

###### Some more operations

<div style="font-size: small; text-align: center">Given a function <code>func</code>, and two lists <code>list1</code> and <code>list2</code>, loop first through <code>list1</code>, then <code>list2</code> and apply the function to the element at that position.</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
givenList ::
  (a -> b -> c) -> List a -> List b -> List c
givenList func list1 list2 =
  applyList (applyList (lift0List func) list1) list2
</code></pre>

---

###### Some more operations

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a

-- if there is a function (a -> b) and a value (a),
-- apply the function; otherwise return no value
applyOptional ::
  Optional (a -> b) -> Optional a -> Optional b
applyOptional Empty _ = Empty
applyOptional _ Empty = Empty
applyOptional (Full f) (Full a) = Full (f a)

lift0Optional :: a -> Optional a
lift0Optional a = Full a
</code></pre>

---

###### Some more operations

<div style="font-size: small; text-align: center">Given a function <code>func</code>, and two values <code>x</code> and <code>y</code>, if either are <code>null</code> return <code>null</code>, otherwise apply the function</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
givenOptional ::
  (a -> b -> c) -> Optional a -> Optional b -> Optional c
givenOptional func x y =
  applyOptional (applyOptional (lift0Optional func) x) y
</code></pre>

---

###### Some more operations &mdash; say what?

<pre><code class="language-haskell hljs" data-trim data-noescape>
givenList ::
  (a -> b -> c) -> List a -> List b -> List c
givenList func list1 list2 =
  applyList (applyList (lift0List func) list1) list2

givenOptional ::
  (a -> b -> c) -> Optional a -> Optional b -> Optional c
givenOptional func x y =
  applyOptional (applyOptional (lift0Optional func) x) y
</code></pre>

---

###### Some more operations &mdash; say what?

<pre><code class="language-haskell hljs" data-trim data-noescape>
givenList ::
  (a -> b -> c) -> <mark>List</mark> a -> <mark>List</mark> b -> <mark>List</mark> c
givenList func list1 list2 =
  <mark>applyList</mark> (<mark>applyList</mark> (<mark>lift0List</mark> func) list1) list2

givenOptional ::
  (a -> b -> c) -> <mark>Optional</mark> a -> <mark>Optional</mark> b -> <mark>Optional</mark> c
givenOptional func x y =
  <mark>applyOptional</mark> (<mark>applyOptional</mark> (<mark>lift0Optional</mark> func) x) y
</code></pre>

---

###### Some more operations &mdash; factor out differences

<pre><code class="language-haskell hljs" data-trim data-noescape>
class CanApplyAndLift0 k where
  apply :: k (a -> b) -> k a -> k b
  lift0 :: a -> k a

instance CanApplyAndLift0 List where
instance CanApplyAndLift0 Optional where
</code></pre>

---

###### Some more operations &mdash; generalise the operation

<pre><code class="language-haskell hljs" data-trim data-noescape>
given ::
  CanApplyAndLift0 k =>
  (a -> b -> c) -> k a -> k b -> k c
given func x y =
  apply (apply (lift0 func) x) y
</code></pre>

* this derived operation works on *anything* that can `apply` and `lift0`
* `List` and `Optional` are instances of `CanApplyAndLift0`

---

###### Some more operations &mdash; say what?

<pre><code class="language-haskell hljs" data-trim data-noescape>
given ::
  CanApplyAndLift0 k =>
  (a -> b -> c) -> k a -> k b -> k c
given func x y =
  apply (apply (lift0 func) x) y
</code></pre>

* this derived operation works on *anything* that can `apply` and `lift0`
* `List` and `Optional` are instances of `CanApplyAndLift0`

---

###### Real names

* `CanApplyAndLift0` is more commonly known as `Applicative`
* `apply` is more commonly known as an infix operator `(<*>)`
* `lift0` is more commonly known as `pure`

---

###### Real names

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Applicative k where
  (<*>) :: k (a -> b) -> k a -> k b
  pure :: a -> k a

instance Applicative List where
instance Applicative Optional where
</code></pre>

---

###### Importantly

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Applicative k where
  (<*>) :: k (a -> b) -> k a -> k b
  pure :: a -> k a

-- what is the type of?
-- `\f x -> pure f <*> x`
</code></pre>

* all things that are `Applicative` also have `fmap`
* all Applicative instances are also Functor instances
* *"`Applicative` is a subclass of `Functor`"*

---

###### Importantly

<pre><code class="language-haskell hljs" data-trim data-noescape>
class <mark>Functor k =></mark> Applicative k where
  (<*>) :: k (a -> b) -> k a -> k b
  pure :: a -> k a
</code></pre>

* all Applicative instances are also Functor instances
* *"`Applicative` is a subclass of `Functor`"*

---

###### Hot tip &mdash; **Practice**

* FAQ #1: *"What?"*
* Practice is of *utmost importance* to internalise these structures
* Practice is how fluency is achieved
* Ask for help if you are stuck, or need more practice
* Do not be disheartened by getting it wrong &mdash; it's essential to practice
* If you get it wrong, and don't know how to proceed, ask for help
* Never forget `:info` is your friend

---

###### Practice

1. Look up the `Applicative` type-class in standard Haskell
2. What are its laws?

<div style="font-size: xx-large">
<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
data Function a b = Function (a -> b)
data Optional a = Empty | Full a

-- write these instances
instance Applicative List where -- 3.
instance Applicative Optional where -- 4.
instance Applicative (Function a) where -- 5.

-- complete then experiment with these derived operations
leftApply ::
  Applicative k => k b -> k a -> k b
leftApply = _ -- 6.

rightApply ::
  Applicative k => k a -> k b -> k b
rightApply = _ -- 7.

swizzle ::
  Applicative k => List (k x) -> k (List x)
swizzle = _ -- 8.
</code></pre>
</div>
