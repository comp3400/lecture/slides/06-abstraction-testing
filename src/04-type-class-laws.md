## Type-class Laws { data-transition="fade none" }

###### The `Eq` type-class

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- All instances must satisfy
--  (x == y) == (y == x)
--  (x == x) == True
class Eq a where
  (==) :: a -> a -> Bool
</code></pre>

---

## Type-class Laws { data-transition="fade none" }

###### The `Eq` type-class

* The law of symmetry: `(x == y) == (y == x)`
* The law of reflexivity: `(x == x) == True`
* These are **type-class laws**
* They are **not** enforced by the compiler
* Type-class instances are expected to adhere to the laws
* Most type-classes have laws

---

## Type-class Laws { data-transition="fade none" }

###### The `Functor` type-class

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Functor k where
  fmap :: (a -> b) -> k a -> k b
</code></pre>

---

## Type-class Laws { data-transition="fade none" }

###### The `Functor` type-class

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- All instances must satisfy
--  fmap (\a -> a) x == x
--  fmap f (fmap g x) == fmap (\a -> (f (g a))) x
class Functor k where
  fmap :: (a -> b) -> k a -> k b
</code></pre>

---

## Type-class Laws { data-transition="fade none" }

###### The `Functor` type-class has two laws

1. The law of identity:

   `fmap (\a -> a) x == x`
2. The law of composition:

   `fmap f (fmap g x) == fmap (\a -> (f (g a))) x`

