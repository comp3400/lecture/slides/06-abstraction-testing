## Revision { data-transition="fade none" }

###### Code Reuse

* We can factor out code that is common by *spotting the differences*
* We can do this because we are Functional Programming
* Things that can map are called (covariant) functors
* Things that are functors have a kind, type to type

---

###### Code Reuse

* The "type of a type" is called its *kind*
* We can ask GHCi for a type's kind with `:kind`
* Type-classes can have (superclass) constraints
* Subclasses have **fewer instances**, but **more derived operations**
* Superclasses have **more instances**, but **fewer derived operations**

---

