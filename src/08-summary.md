## Summary { data-transition="fade none" }

* `Functor` is a type-class denoting anything that has `fmap`
* `Applicative` is a type-class denoting anything that has `(<*>)` and `pure`
* `Applicative` is a subclass of `Functor`
* Automated testing uses universal quantification (forall)
* `IO` is just another data type, with certain useful type-class instances

---

## Summary

* This type and amount of information can be overwhelming
* The COMP3400 team are acutely aware of this and have experience in helping with it
* There is no substitute for **practice**
* We are here to help you practice effectively
* *Ask questions, practice, repeat*

