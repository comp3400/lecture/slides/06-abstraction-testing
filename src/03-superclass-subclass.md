## Revisiting Superclass/Subclass { data-transition="fade none" }

###### The `Eq` type-class

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Eq a where
  (==) :: a -> a -> Bool

instance Eq Int where ...
instance Eq Bool where ...
instance Eq a => Eq [a] where ...
</code></pre>

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### The `Eq` type-class

* We might say that instances of `Eq` must satisfy certain properties
* <code>(x == y) == (y == x)</code>
* <code>(x == x) == True</code>
* *etc*

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### The `Ord` type-class

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Ordering = LT | EQ | GT
class Ord a where
  compare :: a -> a -> Ordering

instance Ord Int where ...
instance Ord Bool where ...
instance Ord a => Ord [a] where ...
</code></pre>

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### The `Ord` type-class

* We might say that instances of `Ord` must satisfy certain properties
* <code>if (x `compare` y) == LT</code> then <code>(y `compare` x) == GT</code>
* <code>if (x `compare` y) == EQ</code> then <code>(y `compare` x) == EQ</code>
* <code>(x `compare` x) == EQ</code>
* *etc*

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### We might also say

* <code>if (x `compare` y) == LT</code> then <code>(x == y) == False</code>
* <code>if (x `compare` y) == GT</code> then <code>(x == y) == False</code>
* <code>if (x `compare` y) == EQ</code> then <code>(x == y) == True</code>

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### Relationship

* We have established a **relationship** between the `Eq` and `Ord` type-classes
* *"all things that have an `Ord` instance, will also have an `Eq` instance"*
* Or in other words, `Eq` is a superclass of `Ord`

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### Relationship

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Eq a => Ord a where
  compare :: a -> a -> Ordering
</code></pre>

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### Consequences of the Relationship (instances)

* All things that have an `Ord` instance, will also have an `Eq` instance
* However, not all things with an `Eq` instance, necessarily have an `Ord` instance
* *"The set of all things with an `Ord` instance is a **strict subset** of the things that have an `Eq` instance"*

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### Consequences of the Relationship (derived operations)

<pre><code class="language-haskell hljs" data-trim data-noescape>
contains :: ? a => a -> [a] -> Bool
sort     :: ? a => [a] -> [a]
</code></pre>

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### Consequences of the Relationship (derived operations)

* The `contains` operation works on all things that have `Eq` (and therefore, things that have `Ord`)
* The `sort` operation works on all things that have `Ord` (`Eq` is insufficient)
* *"The set of derived operations on all things with an `Ord` instance is a **strict superset** of the set of derived operations on things that have an `Eq` instance"*

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

###### Consequences of the Relationship

* The `Ord` type-class is a strict *tightening* of the constraint
* *"Candidate instances not only must have an `Eq` instance, but must also have an `Ord` instance"*
* This tightening lessens the instances, but *increases* the set of derived operations
* We are trading-off instances for more derived operations, by tightening the constraint
* This concept is one key consideration in type-class design

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

<div style="font-size: small; text-align: center">The <code>Eq</code>/<code>Ord</code> instances and derived operations</div>

<div style="text-align:center">
  <a href="images/superclass-subclass-examples-02.png">
    <img src="images/superclass-subclass-examples-02.png" alt="Example of the superclass/subclass relationship" style="width:920px"/>
  </a>
</div>

---

## Revisiting Superclass/Subclass { data-transition="fade none" }

<div style="font-size: small; text-align: center">more generally</div>

<div style="text-align:center">
  <a href="images/superclass-subclass-labelled-01.png">
    <img src="images/superclass-subclass-labelled-01.png" alt="Superclass/subclass relationship" style="width:920px"/>
  </a>
</div>

---

